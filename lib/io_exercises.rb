# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

require 'stringio'

class NoMoreInput < StandardError

end

def guessing_game
  @stdout = StringIO.new
  our_number = 1 + Random.rand(99)
  input = ask_for_input
  puts input
  number_of_guesses = 1
  while check_if_equal(input, our_number) != true
    input = ask_for_input
    number_of_guesses += 1
  end
  final_out = "#{our_number} is right, you used #{number_of_guesses} guesses"
  @stdout << final_out
  final_out
  $stdout = @stdout
end

def ask_for_input
  @stdout << "guess a number: \n"
  puts("guess a number: ")
  input_here = gets.chomp
  raise NoMoreInput.new() if (input_here.to_i < 1 || input_here.to_i > 100)
  input_here.to_i
end

def check_if_equal(num, ans)
  return true if num == ans
  if num > ans
    @stdout << " too high "
    puts "too high"
  elsif num < ans
    @stdout << " too low "
    puts "too low"
  end
end
